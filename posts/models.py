from django.db import models

class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    created = models.DateTimeField(null=True)

    def __str__(self):
        return f"{self.title}"
